### ЗАДАЧА ###
https://docs.google.com/document/d/1oe7XUkYxCFUPg7ySTmSI7_jX5M2BLvPe5xg3RXv_0DI/edit

### ЗАПУСК ###
* `git clone https://bitbucket.org/shatrovalexey/test33`
* `cd test33`
* `composer install`
* `php -S localhost:8081 -t web`
* `lynx http://localhost:8081/`

### АВТОР ###
Шатров Алексей Сергеевич