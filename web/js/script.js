jQuery( function( ) {
	var $timeout = 6e3 ;
	var $list = jQuery( "#list" ) ;
	var $request = function( ) {
		jQuery.ajax( {
			"type" : "post" ,
			"url" : $list.data( "datasource" ) ,
			"dataType" : "json" ,
			"success" : function( $data ) {
				switch ( $data.state ) {
					case "success" : {
						jQuery( $data.data ).each( function( $item ) {
							var $listItem = jQuery( "<li>" ).text( $item[ 0 ] ) ;

							$list.append( $listItem ) ;
						} ) ;
					}
					default : {
						$restart( ) ;
					}
				}
			} ,
			"error" : $restart
		} ) ;
		var $restart = function( ) {
			setTimeout( $request , $timeout ) ;
		} ;
	} ;

	$request( ) ;
} ) ;