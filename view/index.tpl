<!DOCTYPE html>

<html>
	<head>
		<title>{$config->project->name|htmlspecialchars}</title>
		<meta charset="{$config->charset|htmlspecialchars}">
		<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="/js/script.js"></script>
	</head>
	<body>
		<h1>{$config->project->name|htmlspecialchars}</h1>
		<ul id="list" data-datasource="/?action=Data"></ul>
	</body>
</html>