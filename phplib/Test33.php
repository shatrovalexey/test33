<?php
	/**
	* @subpackage Test23 - фасад задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/

	class Test33 {
		/**
		* @const FIND_FILES string - строка для shell для поиска файлов
		*/
		const FIND_FILES = 'find \'%s\' -name \'*\' -print' ;

		/**
		* @var $dbh \PDO - соединение с СУБД
		* @var $file_path string - путь к директории для поиска файлов
		* @var $data array - список найденных файлов в директории $file_path
		*/
		protected $config ;
		protected $dbh ;
		protected $file_path ;
		protected $data ;

		/**
		* В конструкторе выполняются методы create и loadData.
		* @param $dbh \PDO - соединение с СУБД
		* @param $file_name string - имя файла SQL для ресоздания структуры БД
		* @param $file_path string - путь к директории для поиска файлов
		*/
		public function __construct( $file_name ) {
			$this->_prepare( $file_name )->create( )->loadData( ) ;
		}

		/**
		* Загрузка настроек
		* @param $file_name string - имя файла настроек
		* @return $this \Test23
		*/
		private function _prepare( $file_name ) {
			$this->config = $this->_config( $file_name ) ;
			$this->config->project = $this->_config( $this->config->project ) ;
			$this->_connection( ) ;

			return $this ;
		}

		/**
		* Загрузка настроек из файла
		* @param $file_name string - имя файла настроек
		* @return stdClass
		*/
		private function _config( $file_name ) {
			$file_contents = file_get_contents( $file_name ) ;

			return json_decode( $file_contents ) ;
		}

		/**
		* Подключение у СУБД
		* @return $this \Test23
		*/
		private function _connection( ) {
			$this->dbh = new \PDO( $this->config->db->dsn , $this->config->db->username , $this->config->db->passwd ) ;

			return $this ;
		}

		/**
		* Создает структуру таблиц в MySQL для хранения данных, получаемых методом loadData
		* @param $file_name string - имя файла для создания структуры БД
		* @return $this \Test23
		*/
		private function create( ) {
			$this->dbh->setAttribute( \PDO::ATTR_EMULATE_PREPARES , 0 ) ;
			foreach ( $this->config->db->sql as $sql ) {
				$this->dbh->query( $sql ) ;
			}

			return $this ;
		}

		/**
		* находит в директории /tmp/files и всех ее поддиректориях все файлы, имена которых состоят из цифр и букв латинского алфавита, имеют расширение отличное от .exe, и заполнит таблицу filelist данными об этих файлах и структуре папок. Задание должно быть выполнено с использованием регулярных выражений.
		* @return $this \Test23
		*/
		private function loadData( ) {
			$file_path = sprintf( $this->config->sh->find_files , $this->config->settings->where_to_find ) ;
			$cmd_result = shell_exec( $file_path ) ;
			$cmd_results = explode( PHP_EOL , $cmd_result ) ;
			$this->data = array_filter( $cmd_results , function( $file_name ) {
				$file_ext = pathinfo( $file_name , PATHINFO_EXTENSION ) ;

				return strToLower( $file_ext ) == 'exe' ;
			} ) ;

			$sth = $this->dbh->prepare( $this->config->db->insert ) ;

			foreach ( $this->data as $file_path ) {
				$sth->bindParam( 1 , $file_path , PDO::PARAM_STR ) ;
				$sth->execute( ) ;
			}

			$sth->closeCursor( ) ;

			return $this ;
		}

		/**
		* возвращает JSON-структуру с данными о файлах, полученными из созданных методом create таблиц MySQL. Папки и файлы должны быть упорядочены по имени по возрастанию.
		* @return string
		*/
		public function getData( ) {
			$result = $this->dbh->query( '
SELECT
	`fl1`.*
FROM
	`filelist` AS `fl1` ;
			' )->fetchAll( \PDO::FETCH_NUM ) ;

			return $result ;
		}

		/**
		* Представление этого класса. В рамках задачи требуется создать один класс и чтобы в нём всё внутри выполнялось.
		*/
		private function _view( ) {
			$view = new \Smarty( ) ;

			foreach ( $this->config->view as $key => $value ) {
				$view->$key = $value ;
			}

			return $view ;
		}

		/**
		* Представление этого класса.
		* @param $file_name string - имя файла шаблона
		* @param $data array - хэш ключ-значение для подстановки в шаблон
		*/
		private function view( $file_name , $data = array( ) ) {
			$smarty = $this->_view( ) ;

			$smarty->assign( 'config' , $this->config ) ;

			foreach ( $data as $key => $value ) {
				$smarty->assign( $key , $value ) ;
			}

			$smarty->display( $file_name ) ;
		}

		/**
		* Управление HTTP-маршрутизацией
		* @param $server array - $_SERVER
		* @param $request array - $_REQUEST
		* @return \Test33
		*/
		public function execute( &$server , &$request ) {
			if ( empty( $request[ 'action' ] ) ) {
				$action = 'index' ;
			} else {
				$action = $request[ 'action' ] ;
			}

			$prefix = 'ctrl' ;
			$method = $prefix . $action ;

			if ( ! method_exists( $this , $method ) ) {
				$method = $prefix . '500' ;
			}

			$this->$method( $server , $request ) ;

			return $this ;
		}

		/**
		* Контроллер страницы по-умолчанию "/"
		* @param $server array - $_SERVER
		* @param $request array - $_REQUEST
		*/
		private function ctrlIndex( &$server , &$request ) {
			$this->view( 'index.tpl' ) ;
		}

		/**
		* Контроллер страницы с данными "/?action=Data"
		* @param $server array - $_SERVER
		* @param $request array - $_REQUEST
		*/
		private function ctrlData( &$server , &$request ) {
			header( 'Content-Type: application/json; charset=' . $this->config->charset ) ;

			$result = array(
				'state' => 'success' ,
				'data' => $this->getData( )
			) ;

			if ( empty( $result[ 'data' ] ) ) {
				$result[ 'state' ] = 'error' ;
			}

			echo json_encode( $result ) ;
		}

		/**
		* Контроллер страницы с ошибкой HTTP 500
		* @param $server array - $_SERVER
		* @param $request array - $_REQUEST
		*/
		private function ctrl500( &$server , &$request ) {
			header( 'HTTP/1.0 500 Internal server error' ) ;
		}
	}